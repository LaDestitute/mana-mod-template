package ladestitute.manatemplate.capability;

import ladestitute.manatemplate.network.ClientboundPlayerManaUpdateMessage;
import ladestitute.manatemplate.network.SimpleNetworkHandler;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.network.PacketDistributor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class ManaCapability {

    //Using a float makes the meter behave strangely, use ints instead if you copy the example
    //As expected with ints, you'll have to use whole numbers, 1 for half-sphere, 2 for a whole
    //I.e, 6 translates to three spheres, 5 translates to two and a half spheres
    //Visually, going over 20 will probably act as a 'reserve', GUIs will only deplete when they hit 20 or below
    private int maxMana = 20;
    private int currentMana = 0;

    public void addMana(Player player, int amount) {
        if (this.currentMana < this.maxMana) {
            this.currentMana += amount;
        }
        //Failsafe to prevent overflowing over the max-value int
        else this.currentMana = this.maxMana;
        manaClientUpdate(player);
    }

    public void subtractMana(Player player, int amount) {
        if (this.currentMana > 0) {
            this.currentMana -= amount;
        }
        //Failsafe to prevent underflowing
        //There may be a small chance it will underflow to -1 but
        //It will always usually self-correct to 0 next update if whenever
        else this.currentMana = 0;
        manaClientUpdate(player);
    }

    //This is the only method where you may need to manually sync like the example commented out in the eventhandler
    public void setMana(int amount) {
        this.currentMana = amount;
    }

    public int getMana() {
        return currentMana;
    }

    public int getmaxMana() {
        return maxMana;
    }

    //A method for sending a packet to the client to sync information for updating GUis/etc
    //This is done in the capability so this method is
    //included in any methods that add/subtract/set mana to make syncing easier
    public static void manaClientUpdate(Player player) {
        if (!player.level.isClientSide()) {
            ServerPlayer serverPlayer = (ServerPlayer) player;
            player.getCapability(Provider.PLAYER_MANA).ifPresent(cap ->
                    SimpleNetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> serverPlayer),
                            new ClientboundPlayerManaUpdateMessage(cap.currentMana)));
        }
    }

    public static Tag writeNBT(Capability<ManaCapability> capability, ManaCapability instance, Direction side) {
        CompoundTag tag = new CompoundTag();
        tag.putInt("currentmana", instance.getMana());
        return tag;
    }

    public static void readNBT(Capability<ManaCapability> capability, ManaCapability instance, Direction side, Tag nbt) {
        instance.setMana(((CompoundTag) nbt).getInt("currentmana"));
    }

    public static class Provider implements ICapabilitySerializable<Tag> {
        public static Capability<ManaCapability> PLAYER_MANA = CapabilityManager.get(new CapabilityToken<>(){});

        @Nonnull
        private final ManaCapability instance;

        private final LazyOptional<ManaCapability> handler;

        public Provider() {
            instance = new ManaCapability();
            handler = LazyOptional.of(this::getInstance);
        }

        @Nonnull
        @Override
        public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
            return PLAYER_MANA.orEmpty(cap, handler);
        }

        public void invalidate() {
            handler.invalidate();
        }


        public ManaCapability getInstance() {
            return instance;
        }

        @Override
        public Tag serializeNBT() {
            return ManaCapability.writeNBT(PLAYER_MANA, instance, null);
        }

        @Override
        public void deserializeNBT(Tag nbt) {
            ManaCapability.readNBT(PLAYER_MANA, instance, null, nbt);
        }
    }

}
