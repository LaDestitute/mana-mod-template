package ladestitute.manatemplate;

import ladestitute.manatemplate.network.SimpleNetworkHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod("manatemplate")
public class ManaTemplate {
    public static final String MOD_ID = "manatemplate";

    public ManaTemplate() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);

    }

    public void setup(final FMLCommonSetupEvent event) {
        event.enqueueWork(SimpleNetworkHandler::init);
    }
}
