package ladestitute.manatemplate;

import ladestitute.manatemplate.capability.ManaCapability;
import ladestitute.manatemplate.capability.ManaCapabilityHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

@Mod.EventBusSubscriber(modid = ManaTemplate.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class CommonEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticCommonSetup(FMLCommonSetupEvent event) {

        MinecraftForge.EVENT_BUS.register(new ManaCapabilityHandler());

    }

    //The proper way to register capabilities in 1.18 onward
    @SubscribeEvent
    public void registerCaps(RegisterCapabilitiesEvent event) {
        event.register(ManaCapability.class);
    }

}

