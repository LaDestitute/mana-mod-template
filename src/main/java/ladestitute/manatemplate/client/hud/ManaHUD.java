package ladestitute.manatemplate.client.hud;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import ladestitute.manatemplate.ManaTemplate;
import ladestitute.manatemplate.capability.ManaCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = ManaTemplate.MOD_ID)
public class ManaHUD extends GuiComponent {
    public final static ResourceLocation MANA = new ResourceLocation(ManaTemplate.MOD_ID, "textures/gui/hud/icons.png");
    protected final static int WIDTH = 9;
    protected final static int HEIGHT = 9;

    protected Minecraft mc;

    public ManaHUD(Minecraft mc) {
        this.mc = mc;

    }

    public void render(PoseStack poseStack, int screenWidth, int screenHeight) {
        poseStack.pushPose();
        RenderSystem.enableBlend();
        RenderSystem.setShaderTexture(0, MANA);
        mc.player.getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(info -> {
        int mana = info.getMana();
        int OffsetX = screenWidth / 2 + 91;
        int OffsetY = screenHeight - 50;
        int texU = 0;
        int texV = 0;
        // info.getmaxMana()/2 is done instead of "10" so it can scale to mana above 20
            for (int i = 0; i < info.getmaxMana()/2; ++i) {
            int OffsetX1 = OffsetX - i * 8 - 1;
            this.blit(poseStack, OffsetX1, OffsetY, 36 + texU, texV, WIDTH, HEIGHT);

            if (i * 2 + 1 < mana) {
                this.blit(poseStack, OffsetX1, OffsetY, texU, texV, WIDTH, HEIGHT);
            }
            if (i * 2 + 1 == mana) {
                this.blit(poseStack, OffsetX1, OffsetY, texU + 9, texV, WIDTH, HEIGHT);
            }


        }
        });
        poseStack.popPose();
    }
}

