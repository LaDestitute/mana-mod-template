package ladestitute.manatemplate.event;

import ladestitute.manatemplate.ManaTemplate;
import ladestitute.manatemplate.capability.ManaCapability;
import ladestitute.manatemplate.network.ClientboundPlayerManaUpdateMessage;
import ladestitute.manatemplate.network.SimpleNetworkHandler;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.network.PacketDistributor;

@Mod.EventBusSubscriber(modid = ManaTemplate.MOD_ID)
public class ManaEventHandler {

    @SubscribeEvent
    public static void onPlayerEventClone(PlayerEvent.Clone event) {
        boolean flag;
        flag = !(event.getPlayer() instanceof FakePlayer) && event.getPlayer() instanceof ServerPlayer;
        if (flag && event.getPlayer().getCapability(ManaCapability.Provider.PLAYER_MANA).isPresent()) {
            event.getPlayer().getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(date -> {
                event.getOriginal().getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(t -> {
                    date.setMana(t.getMana());
                });
            });
            event.getPlayer().getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(t -> SimpleNetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer) event.getPlayer()), new ClientboundPlayerManaUpdateMessage(t.getMana())));
        }
    }

    //If you want your capability to sync to client properly, then you must send it in
    //this event and whenever you modify cap-values in other events
    //You could also alternatively check PlayerLoggedInEvent, PlayerRespawnEvent, PlayerChangedDimensionEvent
    //but that's three times the work, entityjoinworld covers all three
    @SubscribeEvent
    public static void onEntityJoinWorld(EntityJoinWorldEvent event) {
        if (event.getEntity() instanceof ServerPlayer && !(event.getEntity() instanceof FakePlayer)) {
            event.getEntity().getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(t ->
                    SimpleNetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer) event.getEntity()),
                            new ClientboundPlayerManaUpdateMessage(t.getMana())));
        }
    }

    //An example of how to modify the values in an event
    @SubscribeEvent
    public static void livingDamageEvent(LivingDamageEvent event) {
        //Check if is player doing the damage.
        if (event.getSource().getDirectEntity() instanceof Player) {
            event.getSource().getDirectEntity().getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(data ->
            {
             data.addMana((Player) event.getSource().getDirectEntity(), 1);
             System.out.println("CAP VALUE IS: " + data.getMana());
             //Codeblock to client-sync, not required if you take care of the syncing in your capability class
           //     if (event.getSource().getDirectEntity() instanceof ServerPlayer && !(event.getSource().getDirectEntity() instanceof FakePlayer)) {
              //      event.getSource().getDirectEntity().getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(t ->
              //          SimpleNetworkHandler.CHANNEL.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer) event.getSource().getDirectEntity()),
               //                 new PlayerManaMessage(t.getMana())));
         //   }
        });
    }
    }
}
