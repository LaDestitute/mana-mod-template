package ladestitute.manatemplate.client.hud;

import ladestitute.manatemplate.ManaTemplate;
import ladestitute.manatemplate.capability.ManaCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = ManaTemplate.MOD_ID)
public class HUDHandler {
    private final static ManaHUD MANA_HUD = new ManaHUD(Minecraft.getInstance());

    @SubscribeEvent(receiveCanceled = true)
    public static void onRenderGameOverlayEvent(RenderGameOverlayEvent.Post event) {
        int screenHeight = event.getWindow().getGuiScaledHeight();
        int screenWidth = event.getWindow().getGuiScaledWidth();
        LocalPlayer playerEntity = Minecraft.getInstance().player;
        //render mana
        if (event.getType() == RenderGameOverlayEvent.ElementType.LAYER) {
         //   if (playerEntity != null && !playerEntity.isCreative() && !playerEntity.isSpectator() && !mc.options.hideGui) {
           //     playerEntity.getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(cap -> {
           //         MANA_HUD.render(event.getMatrixStack(), screenWidth, screenHeight);
          //      });
         //   }
            //For testing purposes, comment/remove this and use the above one
            if (playerEntity != null) {
                playerEntity.getCapability(ManaCapability.Provider.PLAYER_MANA).ifPresent(cap -> {
                    MANA_HUD.render(event.getMatrixStack(), screenWidth, screenHeight);
                });
            }
        }

    }
}
