package ladestitute.manatemplate.network;

import ladestitute.manatemplate.capability.ManaCapability;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientboundPlayerManaUpdateMessage implements INormalMessage {
    int Mana;

    public ClientboundPlayerManaUpdateMessage(int mana) {
        this.Mana = mana;
    }

    public ClientboundPlayerManaUpdateMessage(FriendlyByteBuf buf) {
        Mana = buf.readInt();
    }

    @Override
    public void toBytes(FriendlyByteBuf buf) {
            buf.writeInt(Mana);
    }

    @Override
    public void process(Supplier<NetworkEvent.Context> context) {
        //This method is for when information is received by the intended end (i.e, client in this case)
        //We can ignore login to server/client for NetworkDirection, its used for internal forge stuff
        //Remember that client/server side rules apply here
        //Access client stuff only in client, otherwise you will crash MC
        if (context.get().getDirection() == NetworkDirection.PLAY_TO_CLIENT) {
            context.get().enqueueWork(() -> Minecraft.getInstance().player.getCapability(ManaCapability.Provider.PLAYER_MANA).
                    ifPresent(cap -> {
                        //do stuff with the info, such as mainly syncing info for the client-side gui
                cap.setMana(Mana);
            }));
        }
    }
}
